# Go语言内置的RPC框架


```sh
curl --location 'http://localhost:1234/jsonrpc' \
--header 'Content-Type: application/json' \
--data '{
    "method": "HelloService.Hello",
    "params": [
        "bob"
    ],
    "id": 1
}'
```