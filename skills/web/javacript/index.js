// 直接导出 导入
// 变量的解构赋值
import { firstName, lastName, year} from './profile.js'
console.log(firstName, lastName, year)


// 倒出为一个变量,  引用
// import { MYAPP } from './profile.js'
// console.log(MYAPP.foo())

//  快捷写法: 
// import { defaut as user_pkg_name } from './profile.js'
import pkg from './profile.js'
console.log(pkg.foo())



// for of
var o = {
    name: 'Jack',
    age: 20,
    city: 'Beijing'
};
for (var key of Object.keys(o)) {
    console.log(key); // 'name', 'age', 'city'
}

// for of
var array01 = [1,2,3,4]
for (var element of array01) {
    console.log(element)
}

//  并行
array01.forEach((element, i, a) => {
    console.log(element, i, a)
})

// 同时触发10个函数调用
// function myForEach(callbackfn) {
//     for (var element of array01) {
//         callbackfn(element)
//     }
// }
// myForEach((element)=>{
//     console.log(element)
// })

Promise对象


// 函数不能阻塞，需要立即返回
var QueryBlog = (cb) => {
    // 网络Io 后门异步任务   
    // block io : resp--> array01 [] 
    cb(cb)
}

// go (block io)
// array01, err := vblog.QueryBlog()
QueryBlog((array01) => {
    // 处理array01
})

