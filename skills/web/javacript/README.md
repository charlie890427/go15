# Javascript 入门

[Javascript 入门](https://gitee.com/infraboard/go-course/blob/master/day19/javascript.md)

## 环境准备

参考: [前端开发准备](https://gitee.com/infraboard/go-course/blob/master/extra/devcloud/setup.md)

需要多JavaScript版本, 推荐使用nvm

```sh
nvm install 16
cd project
nvm use 16
```

