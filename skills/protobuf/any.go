package protobuf

import (
	"fmt"

	"google.golang.org/protobuf/types/known/anypb"
)

func AnyTest() {
	a := Event{
		Type: EVNET_TYPE_ALERT,
	}

	// 赋值(Server)
	req := &CreateUserRequest{}
	// a.Data = a
	a.Data, _ = anypb.New(req)

	// 取值(Client)
	req2 := &CreateUserRequest{}
	err := a.Data.UnmarshalTo(req2)
	if err != nil {
		panic(err)
	}

	fmt.Println(a)
}
