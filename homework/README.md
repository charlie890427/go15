# 作业

只收录vblog项目, 需要完成后端和前端, 评分标准:
+ 后端 10分, 有自己写文档 2分, 能完成额为补充 2分
+ 前端 10分, 能完成额为补充 2分
+ 独立部署 能够访问 10 分

提交要求: 你们自己提交MR过来合并
+ [学号-名](使用Gitee仓库的地址)
+ [Go15037-小窗](https://github.com/qinchi-ops/govlog) (10 + 1 + 10 + 2)
+ [Go15047-奕丞](https://git.iotroom.top/aenjoy/blogs-system) (10 + 10)
+ [Go15002-Big_程](https://gitee.com/big-cheng/vblog) (10 + 10)
+ [Go14015-卫杰](https://gitee.com/shiwjlinux/go15/tree/master/vblog) (10 + 10)
+ [Go15026-付](https://gitee.com/fuzzp/go15026-fu-vblog) (10 + 10)
+ [Go15021-李杰](https://gitlab.com/lijet) (10 + 10)
+ [GO15018-姜](https://gitee.com/charlie_sh/vblog) (10)
+ [GO15001-韩](https://gitee.com/hbsre/vblog) (10)
+ [Go15033-wangmw](https://gitlab.com/go153534623/vblog) (5)
+ [GO15038-赖](https://gitee.com/coinight/go15) (5)
+ [GO15025-邓](https://gitee.com/dengliyao/vblog.git) (10 + 2)
+ [Go15030-cuilei](https://gitee.com/cuishanjia/go15030-beijing-cui-lei) (无)
+ [Go15014-科](https://gitee.com/cheng-diyang/golang_text/tree/master/vblog) (无)