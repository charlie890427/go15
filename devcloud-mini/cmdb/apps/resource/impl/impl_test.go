package impl_test

import (
	"context"

	"github.com/infraboard/mcube/v2/ioc"
	"gitlab.com/go-course-project/go15/devcloud-mini/cmdb/apps/resource"

	_ "gitlab.com/go-course-project/go15/devcloud-mini/cmdb/apps"
)

var (
	ctx = context.Background()
	svc = resource.GetService()
)

func init() {
	ioc.DevelopmentSetup()
}
