package main

import (
	"github.com/infraboard/mcube/v2/ioc/server/cmd"

	_ "gitlab.com/go-course-project/go15/devcloud-mini/mcenter/apps"

	// 开启API Doc
	// docker run -p 80:8080 swaggerapi/swagger-ui
	_ "github.com/infraboard/mcube/v2/ioc/apps/apidoc/restful"
	// 引入CORS组件
	_ "github.com/infraboard/mcube/v2/ioc/config/cors/gorestful"

	// 导出初始化命令
	"github.com/infraboard/mcenter/cmd/initial"

	// mcenter 复制自己的认证
	_ "github.com/infraboard/mcenter/middlewares/endpoint"
	_ "github.com/infraboard/mcenter/middlewares/grpc"
	_ "github.com/infraboard/mcenter/middlewares/http"
)

func main() {
	cmd.Root.AddCommand(initial.Cmd)

	cmd.Start()

}
