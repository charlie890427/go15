package consumer

import (
	"context"

	"github.com/infraboard/mcube/v2/ioc"
	"github.com/infraboard/mcube/v2/ioc/config/kafka"
	"github.com/infraboard/mcube/v2/ioc/config/log"
	"github.com/rs/zerolog"
	kafka_go "github.com/segmentio/kafka-go"
	"gitlab.com/go-course-project/go15/devcloud-mini/maudit/apps/event"
)

func init() {
	ioc.Default().Registry(&impl{})
}

// 消息kafka里面的事件, 调用controller进行保存
type impl struct {
	ioc.ObjectImpl

	// 模块子Logger
	log *zerolog.Logger
	// svc controller
	svc event.Service
	// consumer对象
	consumer *kafka_go.Reader

	// 这个对象会被托管到ioc
	// ioc 支持对象的 配置(读取配置文件加载: [event_consumer])
	// 直接从配置文件加载配置
	// GROUP_ID ==> EVNET_CONSUMER_GROUP_ID
	ConsumerGroupId string `json:"consumer_group_id" yaml:"consumer_group_id" toml:"consumer_group_id" env:"GROUP_ID"`
	// topic名称
	TopicName string `json:"topic_name" yaml:"topic_name" toml:"topic_name" env:"TOPIC_NAME"`
}

func (i *impl) Name() string {
	return "event_consumer"
}

// 对象销毁时需要释放的资源
// 服务停止的时候, 会自动掉用ioc的每个元素的close方法
func (i *impl) Close(ctx context.Context) error {
	if i.consumer != nil {
		return i.consumer.Close()
	}
	return nil
}

func (i *impl) Init() error {
	// 对象
	i.log = log.Sub(i.Name())
	i.svc = event.GetService()

	// 消息消费者
	i.consumer = kafka.ConsumerGroup(i.ConsumerGroupId, []string{i.TopicName})

	// 启动
	go i.HandleEvent(context.Background())
	return nil
}
