package main

import (
	"github.com/infraboard/mcube/v2/ioc/server/cmd"

	_ "gitlab.com/go-course-project/go15/devcloud-mini/maudit/apps"

	// 开启API Doc
	// docker run -p 80:8080 swaggerapi/swagger-ui
	_ "github.com/infraboard/mcube/v2/ioc/apps/apidoc/restful"
	// 引入CORS组件
	_ "github.com/infraboard/mcube/v2/ioc/config/cors/gorestful"
)

func main() {
	cmd.Start()
}
